<table class="table table-striped tbl" id="companies">
    <thead>
    <tr class="bg-primary">
        <th><a href="{{ url('/companies') }}" class="sort">
                <span id="name" class="glyphicon glyphicon-triangle-bottom sg" aria-hidden="true"> </span>Name </a>
        </th>
        <th><a href="{{ url('/companies') }}" class="sort"> <span id="quota"
                                                                  class="glyphicon glyphicon-triangle-bottom sg"
                                                                  aria-hidden="true"> </span>Quota</a>
        </th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($companies as $company)
        <tr>
            <td>
                <div class="company_field"> {{ $company->name }} </div>
            </td>
            <td>
                <div class="company_field"> {{ $company->quota }} </div>
            </td>
            <td>
                <a href="{{ URL::to('companies/edit/' . $company->id) }}"
                   class="btn btn-sm btn-default company_edit"
                   data-id="{!! $company->id !!}" aria-label="Left Align">
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit
                </a>
                <button type="submit" class="btn btn-sm btn-success company_save"
                        style="float: left; width: 60px; margin-right: 3px;"
                        data-id="{!! $company->id !!}">
                    Save
                </button>
                <a href="{{ URL::to('companies/delete/' . $company->id) }}" class="btn btn-sm btn-default delete"
                   aria-label="Left Align">
                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>delete
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>