@extends('layouts.app', ['current'=>"transfers"])
<div class="col-md-10 col-md-offset-1">
    <div id="page-wrap">
        <table class="table table-striped tbl">
            <thead>
            <tr class="bg-primary">
                <th><a href="{{ url('/transfers') }}" class="sort">
                        <span id="name" class="glyphicon glyphicon-triangle-bottom sg" aria-hidden="true"> </span>User
                        Name </a>
                </th>
                <th><a href="{{ url('/transfers') }}" class="sort">
                        <span id="company" class="glyphicon glyphicon-triangle-bottom sg" aria-hidden="true"> </span>company
                    </a>
                </th>
                <th><a href="{{ url('/transfers') }}" class="sort">
                        <span id="total" class="glyphicon glyphicon-triangle-bottom sg" aria-hidden="true"> </span>total
                    </a>
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach ($transfers as $transfer)
                <tr>
                    <td>{{ $transfer->name }}</td>
                    <td>{{ $transfer->Company->name }}</td>
                    <td>{{ $transfer->total }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $transfers->links() }}
    </div>
</div>