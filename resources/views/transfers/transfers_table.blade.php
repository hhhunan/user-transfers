<table class="table table-striped tbl" id="transferred_logs">
    <thead>
    <tr class="bg-primary">
        <th><a href="{{ url('/transfers_logs') }}" class="sort">
                <span id="id" class="glyphicon glyphicon-triangle-bottom sg" aria-hidden="true"> </span>Id </a></th>
        <th><a href="{{ url('/transfers_logs') }}" class="sort">
                <span id="user.name" class="glyphicon glyphicon-triangle-bottom sg" aria-hidden="true"> </span>User Name
            </a></th>
        <th><a href="{{ url('/transfers_logs') }}" class="sort">
                <span id="resource" class="glyphicon glyphicon-triangle-bottom sg" aria-hidden="true"> </span>Resource
            </a></th>
        <th><a href="{{ url('/transfers_logs') }}" class="sort">
                <span id="date" class="glyphicon glyphicon-triangle-bottom sg" aria-hidden="true"> </span>Date </a></th>
        <th><a href="{{ url('/transferred_logs') }}" class="sort">
                <span id="transferred" class="glyphicon glyphicon-triangle-bottom sg" aria-hidden="true"> </span>Transfer
            </a></th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($transfers as $transfer)
        <tr>
            <td>
                <div class="company_field"> {{ $transfer->id }} </div>
            </td>
            <td>
                <div class="company_field"> {{ $transfer->user->name }} </div>
            </td>
            <td>
                <div class="company_field"> {{ $transfer->resource }} </div>
            </td>
            <td>
                <div class="company_field"> {{ Carbon\Carbon::parse($transfer->date_time)->format('d M Y') }} </div>
            </td>
            <td>
                <div class="company_field"> {{ $transfer->transferred }} </div>
            </td>
            <td>
                <button type="button" class="btn btn-sm btn-default" aria-label="Left Align">
                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>delete
                </button>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>