<table class="table table-striped tbl" id="users">
    <thead>
    <tr class="bg-primary">
        <th><a href="{{ url('/user') }}" class="sort">
                <span id="name" class="glyphicon glyphicon-triangle-bottom sg" aria-hidden="true"> </span>Name
            </a>
        </th>
        <th><a href="{{ url('/user') }}" class="sort"> <span id="email" class="glyphicon glyphicon-triangle-bottom sg"
                                                             aria-hidden="true"> </span>E-mail</a>
        </th>
        <th><a href="{{ url('/user') }}" class="sort"><span id="company_id"
                                                            class="glyphicon glyphicon-triangle-bottom sg"
                                                            aria-hidden="true"> </span>Company</a>
        </th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($users as $user)
        <tr>
            <td>
                <div class="user_field">{{ $user->name }}</div>
            </td>
            <td>
                <div class="user_field">{{ $user->email }}</div>
            </td>
            <td>
                <div class="user_field">{{$user->company->name }}</div>
            </td>
            <td>
                <a href="{{ URL::to('users/edit/' . $user->id) }}" class="btn btn-sm btn-default user_edit"
                   data-id="{!! $user->id !!}" aria-label="Left Align">
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit
                </a>
                <button type="submit" class="btn btn-sm btn-success user_save"
                        style="float: left; width: 60px; margin-right: 3px;" data-id="{!! $user->id !!}">
                    Save
                </button>
                <a href="{{ URL::to('users/delete/' . $user->id) }}" class="btn btn-sm btn-default delete"
                   aria-label="Left Align">
                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>delete
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>