<div class="col-md-12"><input type="text" name="name" class="form-control" value="{{ $user->name }}"></div>
<div class="col-md-12"><input type="text" name="email" class="form-control" value="{{ $user->email }}"></div>
<div class="col-md-12">
    <select name="company" class="form-control" name="company">
        @foreach($companies as $company)
            <option value="{{ $company->id }}" @if($user->company->id == $company->id) selected @endif >
                {{ $company->name }}
            </option>
        @endforeach
    </select>
</div>
