@extends('layouts.app', ['current'=>"users_transfer"])
<div class="col-md-10 col-md-offset-1">
    <div id="page-wrap">
        <table class="table table-striped tbl">
            <thead>
            <tr class="bg-primary">
                <th>Id</th>
                <th>User Name</th>
                <th>Resource</th>
                <th>Date</th>
                <th>Transfer</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($transfers as $transfer)
                <tr>
                    <td>{{ $transfer->id }}</td>
                    <td>{{ $transfer->user->name }}</td>
                    <td>{{ $transfer->resource }}</td>
                    <td>{{ Carbon\Carbon::parse($transfer->date_time)->format('d M Y') }}</td>
                    <td>{{ $transfer->transferred }}</td>
                    <td>
                        <button type="button" class="btn btn-sm btn-default" aria-label="Left Align">
                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit
                        </button>
                        <button type="button" class="btn btn-sm btn-default" aria-label="Left Align">
                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>delete
                        </button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $transfers->links() }}
    </div>
</div>