<?php

use Illuminate\Database\Seeder;
Use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class TransferLogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('App\Transfer');
        for ($i=0; $i < 200; $i++) {
            DB::table('transfer_logs')->insert([
                'user_id' => $faker->numberBetween(1,DB::table('users')->max('id')),
                'date_time' => $faker->dateTime,
                'resource' => $faker->url,
                'transferred' => pow(2, $faker->numberBetween(10,40)),
                'created_at'=>$faker->dateTimeBetween('-5 years'),
                'updated_at'=>$faker->dateTimeBetween('-1 days'),
            ]);
        }
    }
}
