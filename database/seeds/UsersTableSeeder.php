<?php

use Illuminate\Database\Seeder;
Use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('App\Users');
        for ($i=0; $i < 20; $i++) {
            DB::table('users')->insert([
                'name' => $faker->firstName,
                'email' => $faker->unique()->email,
                'company_id' => $faker->numberBetween(1, DB::table('companies')->max('id')),
                'password' => bcrypt($faker->password()),
                'created_at'=>$faker->dateTimeBetween('-5 years'),
                'updated_at'=>$faker->dateTimeBetween('-5 days'),
            ]);
        }
    }
}
