<?php

use Illuminate\Database\Seeder;
Use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('App\Companies');
        for ($i=0; $i < 3; $i++) {
            DB::table('companies')->insert([
                'name' => $faker->company,
                'quota' => $faker->numberBetween(1, 5) . 'TB',
                'created_at'=>$faker->dateTimeBetween('-5 years'),
                'updated_at'=>$faker->dateTimeBetween('-1 days'),
            ]);
        }
    }
}
