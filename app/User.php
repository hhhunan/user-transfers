<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $attributes = [
        'total' => '0'
    ];
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'company_id'
    ];


    /**
     * Get the company that owns the user.
     */
    public function Company()
    {
        return $this->belongsTo('App\Companies');
    }
    /**
     * Get the company that owns the user.
     */
    public function Transfers()
    {
        return $this->hasMany('App\Transfer');
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
