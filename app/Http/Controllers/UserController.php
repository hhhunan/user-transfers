<?php

namespace App\Http\Controllers;

use App\Companies;
use App\Helpers\ConvertHelper;
use App\Http\Requests\UserUpdate;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->input('sort') && $request->input('column')){
            $column = $request->input('column');
            $sort = $request->input('sort');
        }else {
            $column = 'name';
            $sort = 'asc';
        }
        $users=User::orderBy($column, $sort)->paginate(5);
        $view = view('user.user_table', ['users'=>$users]);
        if($request->ajax()){
            return response($view, 200) ->header('Content-Type', 'text/html');
        }
        return view('user.users', ['users'=>$users]);
    }
    /**
        get users transfers
     */
    public function UserTransfers()
    {
        $users=User::with(['Transfers', 'Company'])->paginate(8);
        $users->each(function ($item){;
            return      $item->total= ConvertHelper::convertTransferSizeByType($item->Transfers->sum('transferred'));
        });
        return view('user.user_transfers', ['usertransfers'=>$users, 'current' =>'users']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $user = User::findorfail($id);
        $companies = Companies::all();
        $editView = view('user.edit', ['user'=>$user, 'companies'=>$companies]);
        return response($editView, 200)
            ->header('Content-Type', 'text/html');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdate $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        if(isset($request->company_id)){
            $user->company_id = $request->company_id;
        }
        if($user->save()){
            return response()->json(['message'=>'successfully updated'], 200);
        }else{
            return response()->json(['message'=>'error! not updated'], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       User::destroy($id);
        return response('success', 200);
    }
}
