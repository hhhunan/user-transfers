<?php

namespace App\Http\Controllers;

use App\Companies;
use Illuminate\Http\Request;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->input('sort') && $request->input('column')){
            $column = $request->input('column');
            $sort = $request->input('sort');
        }else {
            $column = 'name';
            $sort = 'asc';
        }
        $companies=Companies::orderBy($column, $sort)->paginate(5);
        $view = view('companies.companies_table', ['companies'=>$companies]);
        if($request->ajax()){
            return response($view, 200)->header('Content-Type', 'text/html');
        }
        return view('companies.companies', ['companies'=>$companies]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $company = Companies::findorfail($id);
        $editView = view('companies.edit', ['company'=>$company]);
        return response($editView, 200)
            ->header('Content-Type', 'text/html');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Companies::find($id);
        $company->name = $request->name;
        $company->quota = $request->quota;
        if($company->save()){
            return response()->json(['message'=>'successfully updated'], 200);
        }else{
            return response()->json(['message'=>'error! not updated'], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Companies::destroy($id);
        return response('success', 200);
    }
    public function getAll()
    {
        $companies = Companies::all();
        return response()->json(['companies'=> $companies], 200);
    }
}
