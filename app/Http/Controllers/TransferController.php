<?php

namespace App\Http\Controllers;

use App\Helpers\ConvertHelper;
use App\Transfer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class TransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->input('sort') && $request->input('column')){
            $column = $request->input('column');
            $sort = $request->input('sort');
        }else {
            $column = 'id';
            $sort = 'asc';
        }
        $transfers=Transfer::orderBy($column, $sort)->paginate(5);
        $view = view('transfers.transfers_table', ['transfers'=>$transfers]);
        if($request->ajax()){
            return response($view, 200)->header('Content-Type', 'text/html');
        }
        return view('transfers.transfers', ['transfers'=>$transfers]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Transfer::destroy($id);
        return response('success', 200);
    }

    /*
     * get all users fransfers
     *
     *@param int $page
     *@param string $column
     *@param string $sort {desc, asc}
     */
    public function usersTransfers(){
        $users=User::with(['Transfers', 'Company'])->paginate(8);
        $users->each(function ($item){
            return      $item->total= ConvertHelper::convertTransferSizeByType($item->Transfers->sum('transferred'));
        });
        return view('transfers.users_transfers', ['transfers'=>$users]);
    }
}
