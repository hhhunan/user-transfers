<?php
/**
 * Created by PhpStorm.
 * User: pk-Hunan
 * Date: 19.06.2017
 * Time: 1:28
 */

namespace App\Helpers;


class ConvertHelper
{
    public static function convertTransferSizeByType($size){
        $types = ['KB'=>pow(2, 10), 'MB'=>pow(2, 20), 'GB'=>pow(2, 30), 'TB'=>pow(2, 40)];
        $typeArray = array_reverse($types);
        foreach ($typeArray as $k =>$v){
            if($size/$v > 1 ){
                $correctType = $k;
                $size = floor($size/$v );
                break;
            }
        }
        if(!$correctType){
            $correctType = 'KB';
        }
        return $size.$correctType;
    }
}