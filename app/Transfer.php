<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    protected $table = 'transfer_logs';

    /**
     * Get the user for transfer.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     *
     * convert normal types format size
     *@params bigint $size
     */


}
