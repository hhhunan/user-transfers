<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'users'], function () {
    Route::get('delete/{id}', ['uses'=>'UserController@destroy']);
    Route::post('/update/{id}', ['uses'=>'UserController@update']);
    Route::get('/edit/{id}', ['uses'=>'UserController@edit']);
    Route::get('/', ['uses'=>'UserController@index']);
});
Route::group(['prefix' => 'companies'], function () {
    Route::get('delete/{id}', ['uses'=>'CompaniesController@destroy']);
    Route::post('update/{id}', ['uses'=>'CompaniesController@update']);
    Route::get('edit/{id}', ['uses'=>'CompaniesController@edit']);
    Route::get('/', ['uses'=>'CompaniesController@index']);
});
Route::get('/transfers', ['uses' => 'TransferController@usersTransfers']);
Route::get('/transferred_logs', ['uses' => 'TransferController@index']);
Route::get('/transferred_logs/delete/{id}', ['uses' => 'TransferController@destroy']);

