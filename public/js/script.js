/**
 * Created by pk-Hunan on 19.06.2017.
 */
$(document).ready(function () {
    $('.alert').hide();
    $('body').find('.user_save').hide();
    $('body').find(' .company_save').hide();
/**
 * user edit
 * */
    $(".user_edit").on("click", function (e) {
        if(($('button').not(':hidden').length >0)){
            $('body').find('.user_field').show();
            $('body').find('.form-control').hide();
            $('body').find('.user_edit').show();
            $('body').find('.user_save').hide();
            $('body').find('.my-error-class').remove();
        }
        var el = e.currentTarget;
        e.preventDefault();
        var url = this;
        var trRow = $('body').find(el).parents('tr');
            $.ajax({
                type: 'GET',
                url: url,
                success: function (response) {
                    var user = $(response);
                    $('body').append("<div class='user_data hide'></div>");
                    var htmlUser = $('.user_data').html(response);
                    var user = htmlUser[0].children;
                    trRow.children('td').each(function (i, v) {
                        $(v).children('.user_field').hide();
                        if(i < user.length){
                            var data = user[i];
                            v.append($.clone(data))
                        }else{
                            $(v).find('.user_edit').hide();
                            $(v).find('.user_save').show();
                        }
                    });
                },
                error: function (error) {
                    alert(error);
                }
            });
    });
/**
 * user update
 * */
    $("#save_user_form").validate({
        errorClass: "my-error-class",
        validClass: "my-valid-class",
        rules: {
            name: {
                required: true
            },
            email: {
                required: true,
                email: true
            }
        },
        submitHandler: function(form) {
            var id = $(form).find('.user_save').attr('data-id');
            var url = "users/update/"+id;
            var name = $('input[name="name"]').val();
            var email = $('input[name="email"]').val();
            var company_id = $('option:selected').val();
            var token = $('input[name="_token"]').val();
            data = {'_token':token, 'name':name,'email': email,'company_id': company_id};
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (response) {
                    alertNotification(response.message, 'success');
                    var tdElements = $('.form-control').parents('td');
                    tdElements.each(function (i,v) {
                       var input = $(v).find('.form-control');
                       var oldVal = $(v).find('.user_field');
                        oldVal.show();
                        if($(input.children()).is('option')){
                            $(v).html(oldVal.html(input.children(':selected')[0].text));
                        }else{
                            $(v).html(oldVal.html(input.val()));
                        }
                    });
                    $('.user_save').hide();
                    $('.user_edit').show();
                },
                error:function (response) {
                    if( response.status === 422 ) {
                        var errors = response.responseJSON;
                        errorsHtml = '<ul>';
                        $.each( errors , function( key, value ) {
                            errorsHtml += '<li>' + value[0] + '</li>';
                        });
                        errorsHtml += '</ul>';
                        $( '.alert' ).addClass('alert-danger');
                        $( '.alert' ).html( errorsHtml );
                        $( '.alert' ).show();

                    } else {
                        alertNotification('Something is wrong', 'error');
                    }
                }
            });
        }
    });
    /**
     * company edit
     * */
    $(".company_edit").on("click", function (e) {
        if(($('button').not(':hidden').length >0)){
            $('body').find('.company_field').show();
            $('body').find('.form-control').hide();
            $('body').find('.company_edit').show();
            $('body').find('.company_save').hide();
            $('body').find('.my-error-class').remove();
        }
        var el = e.currentTarget;
        e.preventDefault();
        var url = this;
        var trRow = $('body').find(el).parents('tr');
        $.ajax({
            type: 'GET',
            url: url,
            success: function (response) {
                var user = $(response);
                $('body').append("<div class='company_data hide'></div>");
                var htmlCompany = $('.company_data').html(response);
                var company = htmlCompany[0].children;
                trRow.children('td').each(function (i, v) {
                    $(v).children('.company_field').hide();
                    if(i < company.length){
                        var data = company[i];
                        v.append($.clone(data))
                    }else{
                        $(v).find('.company_edit').hide();
                        $(v).find('.company_save').show();
                    }
                });
            },
            error: function (error) {
                alert(error);
            }
        });
    });

    /**
     * company update
     * */
    $("#save_company_form").validate({
        errorClass: "my-error-class",
        validClass: "my-valid-class",
        rules: {
            name: {
                required: true
            },
            quota: {
                required: true,
                number: true
            }
        },
        submitHandler: function(form) {
            var id = $(form).find('.company_save').attr('data-id');
            var url = "companies/update/"+id;
            var name = $('input[name="name"]').val();
            var quota = $('input[name="quota"]').val();
            var token = $('input[name="_token"]').val();
            data = {'_token':token, 'name':name,'quota': quota};
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (response) {
                    alertNotification(response.message, 'success');
                    var tdElements = $('.form-control').parents('td');
                    tdElements.each(function (i,v) {
                        var input = $(v).find('.form-control');
                        var oldVal = $(v).find('.company_field');
                        oldVal.show();
                        $(v).html(oldVal.html(input.val()));
                    });
                    $('.company_save').hide();
                    $('.company_edit').show();
                },
                error:function (response) {
                    if( response.status === 422 ) {
                        var errors = response.responseJSON;
                        errorsHtml = '<ul>';
                        $.each( errors , function( key, value ) {
                            errorsHtml += '<li>' + value[0] + '</li>';
                        });
                        errorsHtml += '</ul>';
                        $( '.alert' ).addClass('alert-danger');
                        $( '.alert' ).html( errorsHtml );
                        $( '.alert' ).show();

                    } else {
                        alertNotification('Something is wrong', 'error');
                    }
                }
            });
        }
    });

    /**
     * delete
     * */
    function deleteAction(url)
    {
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                //message
            }
        });
    }
/**
 * alert notifications success or error
 * @params message
 * $params type {error or success}
 * */
    function alertNotification(message, type) {
        $('.alert').show();
        if(type == 'success')
        {
            $('.alert').addClass('alert-success');
            if($('.alert').hasClass('alert-danger')){
                $('.alert').removeClass('alert-danger')
            }
        }
        else
        {
            $('.alert').addClass('alert-danger');
            if($('.alert').hasClass('alert-success')){
                $('.alert').removeClass('alert-success')
            }
        }
        $('.alert').html(message);
        setTimeout(function () {
            $('.alert').hide();
        }, 2500);
    }
    $('body').on('click', '.sort', function (e) {
        e.preventDefault();
        var table = $('table');
        var column = $(this.children).attr('id');
        sort(table, column);
    });
    $('body').on('click', '.delete', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        var row = $(this).parents('tr');
        deleteAction(url)
        row.remove();
    });
    /**
     * Sorting table by column name
     * */
    function sort(table, column) {
        if($(table).find('#'+ column)){
            var col = $(table).find('#'+ column);
            var sort;
            if(col.hasClass('glyphicon-triangle-bottom')){
                sort = "desc";
            }
            else if (col.hasClass('glyphicon-triangle-top')){
                sort = "asc";
            }
            var url = "/" + table.attr('id') + "?" + 'column' + '=' +column + '&sort=' + sort;
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    if(response.length > 0){
                        $('form').find('table').remove();
                        $('form').append(response);
                        if(sort == 'desc'){
                            $('body').find('#'+column).removeClass('glyphicon-triangle-bottom');
                            $('body').find('#'+column).addClass('glyphicon-triangle-top');
                        }else{
                            $('body').find('#'+column).addClass('glyphicon-triangle-bottom');
                            $('body').find('#'+column).removeClass('glyphicon-triangle-top');
                        }
                        $('.company_save').hide();

                    }
                },
            });

        }
    }



});
